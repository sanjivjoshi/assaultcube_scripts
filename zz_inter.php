<?php

$webhookurl = "https://discordapp.com/api/webhooks/701365140309934131/56DA1ryQtg7ocRUEW-M7-6rEhceqS-z6DZWC24L84MfcIfivngkH2GbcnPzt5EKCf5SP";

$timestamp = date("c", strtotime("now"));

$json_data = json_encode([
    "content" => "Inter @everyone", 
    "username" => "Fats",
    "tts" => false,
    "embeds" => [
        [
            "title" => "Calls for inter on ZZ match server",
            "type" => "rich",
            "description" => "Type /connect zzgang.servegame.com 4999 match",
            //"url" => "assaultcube://zzgang.servegame.com:3999/?password=match",
            "timestamp" => $timestamp,
            "color" => hexdec( "3366ff" ),
            "footer" => [
                "text" => "fatso"
            ],
            "image" => [
                "url" => "https://acwceu.servegame.com/inter.jpg"
            ],
            "author" => [
                "name" => $argv[1]
            ]
        ]
    ]

], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE );


$ch = curl_init( $webhookurl );
curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
curl_setopt( $ch, CURLOPT_POST, 1);
curl_setopt( $ch, CURLOPT_POSTFIELDS, $json_data);
curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt( $ch, CURLOPT_HEADER, 0);
curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);

$response = curl_exec( $ch );
curl_close( $ch );