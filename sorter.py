#!/usr/bin/env python3
import json, os.path, datetime
from operator import itemgetter

buffer = ""

def converttime(time):
  min, sec = divmod(time, 60) 
  hour, min = divmod(min, 60) 
  return "%d:%02d:%02d" % (hour, min, sec) 

def setpph(score, time):
  hour, min, sec = time.split(":")
  if int(hour)!= 0:
    return round((score/int(hour)),2)
  elif int(min) != 0:
    return round((score/int(min)*60),2)
  elif int(sec) != 0:
    return round((score/int(sec) * 3600), 2)
  else:
    return 0

def combineDict(toDict):
    for okey in list(toDict.keys()):
        for ikey in list(toDict.keys()):
            if ikey == okey:
                continue
            try:
                if(toDict[ikey]['recentname']==toDict[okey]['recentname'] and toDict[ikey]['country'] == toDict[okey]['country']):
                    toDict[okey]['score'] += toDict[ikey]['score']
                    toDict[okey]['time'] += toDict[ikey]['time']
                    toDict[okey]['deaths'] += toDict[ikey]['deaths']
                    toDict[okey]['frags'] += toDict[ikey]['frags']
                    toDict[okey]['hs'] += toDict[ikey]['hs']
                    toDict[okey]['sl'] += toDict[ikey]['sl']
                    toDict[okey]['gb'] += toDict[ikey]['gb']
                    if(toDict[okey]['deaths'] != 0):
                      toDict[okey]['k/d'] = round(float(toDict[okey]["frags"]) / float(toDict[okey]["deaths"]), 2)
                    else:
                      toDict[okey]["k/d"] = round(float(toDict[okey]["frags"]), 2)
                    toDict[okey]['dm lss osok'] += toDict[ikey]['dm lss osok']
                    toDict[okey]['tdm tlss tosok'] += toDict[ikey]['tdm tlss tosok']
                    toDict[okey]['ctf htf'] += toDict[ikey]['ctf htf']
                    toDict[okey]['tk'] += toDict[ikey]['tk']
                    toDict[okey]['flags'] += toDict[ikey]['flags']
                    toDict[okey]['flagreturns']+=toDict[ikey]['flagreturns']
                    del toDict[ikey]
                else:
                  continue
            except:
                continue
    return toDict

def removeUseless(toDict):
  for key in list(toDict.keys()):
    if(toDict[key]['recentname'] == "unarmed" or toDict[key]['score'] <= 150000 or toDict[key]['time'] < 3600):
      del toDict[key]
  return toDict

if os.path.exists('stats.json'):
    f = open('stats.json', )
    toDict = json.load(f)
    f.close()
    toDict = combineDict(toDict)
    toDict = removeUseless(toDict)
    player = []
    finalDict={}
    for key in toDict.keys():
      time = converttime(int(toDict[key]["time"]))
      player.append({"name":toDict[key]["recentname"], "score":toDict[key]["score"], "deaths":toDict[key]["deaths"], "frags":toDict[key]["frags"], "headshots":toDict[key]["hs"], \
            "gibs":toDict[key]["gb"], "slashes":toDict[key]["sl"], \
                "k/d":toDict[key]["k/d"], "dm lss osok":toDict[key]["dm lss osok"],\
                    "ctf htf":toDict[key]["ctf htf"], "flagreturns":toDict[key]["flagreturns"],\
                        "tdm tlss tosok":toDict[key]["tdm tlss tosok"], "tk":toDict[key]["tk"], "flags":toDict[key]["flags"], "time":time, "pph":setpph(toDict[key]["score"], time), "country":toDict[key]["country"]})

    data_sorted = sorted(player, key=itemgetter("score"), reverse=True)
    count = 0
    buffer = """<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-176045881-1"></script>
  <script>
   window.dataLayer = window.dataLayer || [];
   function gtag(){dataLayer.push(arguments);}
   gtag('js', new Date());
   gtag('config', 'UA-176045881-1');
  </script>

  <meta name="google-site-verification" content="UrAxHOxXYFmk1XcRQxZpWfK_6EWyn5DMJyZltsrfTpM" />
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title></title> 
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css">
  <style>
    th,td{
      text-align: center; 
    }
  </style>
</head>

<body>
  <a href="https://zzgang.servegame.com/demos"><p style="text-align:center">Demos!</p></a>
  <a href="https://zzgang.servegame.com/record.html"><p style="text-align:center">Records</p></a>
  <p> Requires at least 5 players in ladder for the stats to be collected, matches played should be 15 mins only and mastermode must be open. </p>
  <p> Showing top 100 only for faster loading of stats. </p>
  <p> Do not change names, if changing names write to fatso otherwise stats remain uncombined </p>
  <p> Names are tagged against your IP, so if your IP changes and if you continue to use your name itself then the values will be combined automatically, otherwise the values will be uncombined </p>
  <p> Ladder last update time </p>
  """
    buffer = str(buffer) + str(datetime.datetime.now()) + " GMT+1"
    buffer = str(buffer) + \
  """
  <table class="table-striped table-bordered display ordered table-sm" cellspacing="0" width="80%">
    <thead>
      <tr>
        <th>Rank
        </th>
	<th>Country
	</th>
        <th>Name
        </th>
        <th>Score
        </th>
        <th>Frags
        </th>
        <th>Deaths
        </th>
        <th>K/D
        </th>
        <th>Headshots
        </th>
        <th>Slashes
        </th>
        <th>Gibs
        </th>
        <th>Teamkills
        </th>
        <th>Flags
        </th>
        <th>CTF/HTF
        </th>
        <th>FlagReturns
        </th>
        <th>DM/LSS/OSOK
        </th>
        <th>TDM/TLSS/TOSOK
        </th>
        <th>Time
        </th>
        <th>PPH
        </th>
      </tr>
    </thead>
    <tbody>
"""
    for sdic in data_sorted:
        count = count + 1
        buffer = str(buffer) + "<tr><td>"+str(count)+"</td><td>"+str(sdic["country"])+"</td><td>"+str(sdic["name"])+"</td><td>"+str(sdic["score"])+"</td><td>"\
            +str(sdic["frags"])+"</td><td>"+str(sdic["deaths"])+"</td><td>"+str(sdic["k/d"])+"</td><td>"+str(sdic["headshots"])+"</td><td>"+\
                str(sdic["slashes"])+"</td><td>"+str(sdic["gibs"])+"</td><td>"+str(sdic["tk"])+"</td><td>"\
                    +str(sdic["flags"])+"</td><td>"+str(sdic["ctf htf"])+"</td><td>"+str(sdic["flagreturns"])+"</td><td>"\
                        +str(sdic["dm lss osok"])+"</td><td>"+str(sdic["tdm tlss tosok"])+"</td><td>"+str(sdic["time"])+"</td><td>"+str(sdic["pph"])+"</td></tr>"
        if count >= 100:
	        break
    buffer = str(buffer) + """
  
  </tbody>
  </table>
  <!-- End your project here-->
  <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.colVis.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/fixedheader/3.1.7/js/dataTables.fixedHeader.min.js"></script>
  <script>
    $(document).ready(function() {
    var table = $(".ordered").DataTable( {
        "order": [[ 3, "desc" ]],
        "paging":true,
        scrollY : 370,
        scrollX : true,
        info:false,
         dom: 'Bfrtip',
         columnDefs: [
             {
                 targets: 1,
                 className: 'noVis'
             }
         ],
         buttons: [
             {
                 extend: 'colvis',
                 columns: ':not(.noVis)'
             }
         ]
    } );
    $('a.toggle-vis').on( 'click', function (e) {
        e.preventDefault();
        var column = table.column( $(this).attr('data-column') );
        column.visible( ! column.visible() );
    } );
} );
  </script>
</body>

</html>
    """
    f1 = open("/var/www/zzgang.servegame.com/public_html/index.html", "w+")
    f1.write(buffer)
    f1.close()

