#!/usr/bin/env python3
import json
import os.path
import datetime
from operator import itemgetter
import collections


# init
buffer = ""
player = []
banned_names = ['...', 'YacClear', 'unarmed', '.', 'Cutlass;', 'china']

# main driver
if os.path.exists('record.json'):
    f = open('record.json', )
    toDict = json.load(f)
    f.close()
    for host in toDict.keys():
        if(len(toDict[host]["record_hs"]) > 1):
            sorted_dict = collections.OrderedDict(
                sorted(toDict[host]["record_hs"].items(), reverse=True))
            keys = sorted_dict.keys()
            values = sorted_dict.values()
            for key in toDict[host]["record_hs"].keys():
                player.append({
                    "country": toDict[host]["country"],
                    "name": toDict[host]["name"],
                    "score": int(0),
                    "map_sc": "",
                    "record_frags": int(0),
                    "map_fr": "",
                    "record_hs": toDict[host]["record_hs"][key],
                    "hs_map": key,
                    "record_flag": int(0),
                    "map_fl": "",
                    "kd": int(0),
                    "kd_map": ""
                })
        if(len(toDict[host]["record_frags"]) > 1):
            sorted_dict = collections.OrderedDict(
                sorted(toDict[host]["record_frags"].items(), reverse=True))
            keys = sorted_dict.keys()
            values = sorted_dict.values()
            for key in toDict[host]["record_frags"].keys():
                player.append({
                    "country": toDict[host]["country"],
                    "name": toDict[host]["name"],
                    "score": int(0),
                    "map_sc": "",
                    "record_frags": toDict[host]["record_frags"][key],
                    "map_fr": key,
                    "record_hs": int(0),
                    "hs_map": "",
                    "record_flag": int(0),
                    "map_fl": "",
                    "kd": int(0),
                    "kd_map": ""
                })
        if(len(toDict[host]["record_flag"]) > 1):
            sorted_dict = collections.OrderedDict(
                sorted(toDict[host]["record_flag"].items(), reverse=True))
            keys = sorted_dict.keys()
            values = sorted_dict.values()
            for key in toDict[host]["record_flag"].keys():
                player.append({
                    "country": toDict[host]["country"],
                    "name": toDict[host]["name"],
                    "score": int(0),
                    "map_sc": "",
                    "record_frags": int(0),
                    "map_fr": "",
                    "record_hs": int(0),
                    "hs_map": "",
                    "record_flag": toDict[host]["record_flag"][key],
                    "map_fl": key,
                    "kd": int(0),
                    "kd_map": ""
                })
        if(len(toDict[host]["record_sc"]) > 1):
            sorted_dict = collections.OrderedDict(
                sorted(toDict[host]["record_sc"].items(), reverse=True))
            keys = sorted_dict.keys()
            values = sorted_dict.values()
            for key in toDict[host]["record_sc"].keys():
                player.append({
                    "country": toDict[host]["country"],
                    "name": toDict[host]["name"],
                    "score": toDict[host]["record_sc"][key],
                    "map_sc": key,
                    "record_frags": int(0),
                    "map_fr": "",
                    "record_hs": int(0),
                    "hs_map": "",
                    "record_flag": int(0),
                    "map_fl": "",
                    "kd": int(0),
                    "kd_map": ""
                })
        if(len(toDict[host]["kd"]) > 1):
            sorted_dict = collections.OrderedDict(
                sorted(toDict[host]["kd"].items(), reverse=True))
            keys = sorted_dict.keys()
            values = sorted_dict.values()
            for key in toDict[host]["kd"].keys():
                player.append({
                    "country": toDict[host]["country"],
                    "name": toDict[host]["name"],
                    "score": int(0),
                    "map_sc": "",
                    "record_frags": int(0),
                    "map_fr": "",
                    "record_hs": int(0),
                    "hs_map": "",
                    "record_flag": int(0),
                    "map_fl": "",
                    "kd": toDict[host]["kd"][key],
                    "kd_map": key
                })
    data_sorted = sorted(player, key=itemgetter("score"), reverse=True)
    count = 0
    buffer = \
        """
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title></title> 
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css">
  <style>
    th,td{
      text-align: center; 
    }
    body {
      margin: auto;
    }
    div.dataTables_wrapper {
      margin-bottom: 3em;
    }
  </style>
</head>
<body>
  <p> Disclaimer: Information shown here is automatically updated everyday once and information shown here is moderated. However, errors could occur where a hacker's details could be shown as well. Please report to any of the ZZ's or Ladder Admins and we'll take care of the rest. Thanks for these wonderful moments in game. </p>
  <p> Records seen here have to have at least 8 players in a game for a record to be counted. Mastermode is open and you aren't blacklisted. </p>
  <p> This script got last executed at </p>
  """
    buffer = str(buffer) + str(datetime.datetime.now())
    buffer = str(buffer) + \
        """
  <h2> Scorers! </h2>
  <p> <b> You ask scores? Well these players know no bounds of it. They stop at nothing! </b> </p> 

  <table id="" class="display" width="100%">
    <thead>
      <tr>
        <th>Country
        </th>
        <th>Name
        </th>
        <th>Score
        </th>
        <th>Map
        </th>
        </tr>
    </thead>
    <tbody>
"""

    for sdic in data_sorted:
        if str(sdic["map_sc"]) in ('ac_casa', 'ac_village', 'ac_ingress', 'ac_africa', 'ac_syria_3.0', 'ac_shine', 'ac_casa', 'ac_desert3', 'ac_douzectf', 'rapier', 'ac_sunset', 'ac_tripoli') and (str(sdic['name']) not in banned_names):
            count = count + 1
            buffer = str(buffer) +\
                "<tr><td>"+str(sdic["country"]) +\
                "</td><td>"+str(sdic["name"]) +\
                "</td><td>"+str(sdic["score"]) +\
                "</td><td>"+str(sdic["map_sc"]) +\
                "</td></tr>"
            if count > 24:
                break
    buffer = str(buffer) +\
        """
</tbody>
  </table>
  <h2> Headshotters! </h2>
  <p> <b> These players don't see anything in their game but heads! Put a hole in the head? These are some sharpshooters you'll love! </b></p>
  <table id="" class="display" width="100%">
    <thead>
      <tr>
        <th>Country
        </th>
        <th>Name
        </th>
        <th>Headshots
        </th>
        <th>Map
        </th>
        </tr>
    </thead>
    <tbody>
"""
    data_sorted = sorted(player, key=itemgetter("record_hs"), reverse=True)
    count = 0
    for sdic in data_sorted:
        if str(sdic["hs_map"]) in ('ac_casa', 'ac_village', 'ac_ingress', 'ac_africa', 'ac_syria_3.0', 'ac_shine', 'ac_casa', 'ac_desert3', 'ac_douzectf', 'rapier', 'ac_sunset', 'ac_tripoli') and (str(sdic['name']) not in banned_names):
            count = count + 1
            buffer = str(buffer) +\
                "<tr><td>"+str(sdic["country"]) +\
                "</td><td>"+str(sdic["name"]) +\
                "</td><td>"+str(sdic["record_hs"]) +\
                "</td><td>"+str(sdic["hs_map"]) +\
                "</td></tr>"
            if count > 24:
                break
    buffer = str(buffer) +\
        """
  </tbody>
  </table>
  <h2> Fraggers! </h2>
  <p><b> I can still hear the bullet shells fall on the floor. You don't wanna mess as well as miss being killed by these players! </b></p>
  <table id="" class="display" width="100%">
    <thead>
      <tr>
        <th>Country
        </th>
        <th>Name
        </th>
        <th>Frags
        </th>
        <th>Map
        </th>
        </tr>
    </thead>
    <tbody>
"""
    data_sorted = sorted(player, key=itemgetter("record_frags"), reverse=True)
    count = 0
    for sdic in data_sorted:
        if str(sdic["map_fr"]) in ('ac_casa', 'ac_village', 'ac_ingress', 'ac_africa', 'ac_syria_3.0', 'ac_shine', 'ac_casa', 'ac_desert3', 'ac_douzectf', 'rapier', 'ac_sunset', 'ac_tripoli') and (str(sdic['name']) not in banned_names):
            count = count + 1
            buffer = str(buffer) +\
                "<tr><td>"+str(sdic["country"]) +\
                "</td><td>"+str(sdic["name"]) +\
                "</td><td>"+str(sdic["record_frags"]) +\
                "</td><td>"+str(sdic["map_fr"]) +\
                "</td></tr>"
            if count > 24:
                break
    buffer = str(buffer) +\
        """
  </tbody>
  </table>
  <h2> Flaggers! </h2>
  <p><b> Cheers to these flag runners! Don't know how we ever won the CTF/HTF/KTF matches without these players!</b></p>
  <table id="" class="display" width="100%">
    <thead>
      <tr>
        <th>Country
        </th>
        <th>Name
        </th>
        <th>Flags
        </th>
        <th>Map
        </th>
        </tr>
    </thead>
    <tbody>
"""
    data_sorted = sorted(player, key=itemgetter("record_flag"), reverse=True)
    count = 0
    for sdic in data_sorted:
        if str(sdic["map_fl"]) in ('ac_casa', 'ac_village', 'ac_ingress', 'ac_africa', 'ac_syria_3.0', 'ac_shine', 'ac_casa', 'ac_desert3', 'ac_douzectf', 'rapier', 'ac_sunset', 'ac_tripoli') and (str(sdic['name']) not in banned_names):
            count = count + 1
            buffer = str(buffer) +\
                "<tr><td>"+str(sdic["country"]) +\
                "</td><td>"+str(sdic["name"]) +\
                "</td><td>"+str(sdic["record_flag"]) +\
                "</td><td>"+str(sdic["map_fl"]) +\
                "</td></tr>"
            if count > 24:
                break
    buffer = str(buffer) +\
        """
  </tbody>
  </table>
  <h2> K/Ds! </h2>
  <p><b> Nerves of steel! You say 241 or nothing? How about some everything or nothing?</b></p>
  <table id="" class="display" width="100%">
    <thead>
      <tr>
        <th>Country
        </th>
        <th>Name
        </th>
        <th>K/D
        </th>
        <th>Map
        </th>
        </tr>
    </thead>
    <tbody>
"""
    data_sorted = sorted(player, key=itemgetter("kd"), reverse=True)
    count = 0
    for sdic in data_sorted:
        if str(sdic["kd_map"]) in ('ac_casa', 'ac_village', 'ac_ingress', 'ac_africa', 'ac_syria_3.0', 'ac_shine', 'ac_casa', 'ac_desert3', 'ac_douzectf', 'rapier', 'ac_sunset', 'ac_tripoli') and (str(sdic['name']) not in banned_names):
            count = count + 1
            buffer = str(buffer) +\
                "<tr><td>"+str(sdic["country"]) +\
                "</td><td>"+str(sdic["name"]) +\
                "</td><td>"+str(sdic["kd"]) +\
                "</td><td>"+str(sdic["kd_map"]) +\
                "</td></tr>"
            if count > 24:
                break
    buffer = str(buffer) +\
        """
  </tbody>
  </table>
  <!-- End your project here-->
  <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.colVis.min.js"></script>
  <script type="text/javascript" src="https://cdn.datatables.net/fixedheader/3.1.7/js/dataTables.fixedHeader.min.js"></script>
  <script>
    $(document).ready(function() {
    var t = $("table.display").DataTable( {
	"columnDefs": [ {
            "searchable": false,
            "orderable": false,
            "targets": 0
        } ],
        "order": []
    } );
} );
  </script>
</body>

</html>
"""
    f1 = open("/var/www/zzgang.servegame.com/public_html/record.html", "w+")
    f1.write(buffer)
    f1.close()
