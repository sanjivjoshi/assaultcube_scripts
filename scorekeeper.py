import time, re, json, os.path, collections, operator, fcntl
import IP2Location

IP2LocObj = IP2Location.IP2Location()

match_ctf = False
match_dm = False
match_tdm = False
Dict = {}
record_or_not = False
write_to_dict = False
IP2LocObj.open("/home/assaultcube/Common-Data/IP2Location/IP-COUNTRY.BIN")

def setcountry(rec):
    if rec is not None:
        try:
            return rec.country_short
        except:
            return ""
    else:
        return ""

def dictionary(Dict, host, name):
    rec = IP2LocObj.get_all(host)
    if os.path.exists('stats.json') and Dict =={}:
        with open("stats.json", "r") as g:
            Dict = json.load(g)
            g.close()
        if host in Dict.keys():
            Dict[host]["recentname"] = name
            return Dict
        else:
            Dict[host] = {"recentname":name, "score":int(0), "frags":int(0), "deaths":int(0), \
                "k/d":float(0), "hs":int(0), "sl":int(0), "gb":int(0), "ctf htf":int(0), \
                    "tdm tlss tosok":int(0), "dm lss osok":int(0), "tk":int(0), "flagreturns": int(0), "flags":int(0), "time":int(0), "country":setcountry(rec)}
            return Dict
    elif host in Dict.keys():
        Dict[host]["recentname"] = name
        return Dict
    else:
        Dict[host] = {"recentname": name, "score":int(0), "frags":int(0), "deaths":int(0), \
            "k/d":float(0), "hs":int(0), "sl":int(0), "gb":int(0), "ctf htf":int(0), \
                "tdm tlss tosok":int(0), "dm lss osok":int(0), "tk":int(0), "flagreturns":int(0), "flags":int(0), "time":int(0), "country":setcountry(rec)}
    return Dict

file = open('zz1.log', 'r')

def update(info, host, name, Dict):
    Dict = dictionary(Dict, host, name)
    Dict[host]["recentname"] = info[1]
    size = len(info)
    if size == 11:
        Dict[host]["flags"] =  Dict[host]["flags"] + int(info[3])
        Dict[host]["score"] = Dict[host]["score"] + int(info[4])
        Dict[host]["frags"] = Dict[host]["frags"] + int(info[5])
        Dict[host]["deaths"] = Dict[host]["deaths"] + int(info[6]) 
        Dict[host]["tk"] = Dict[host]["tk"] + int(info[7])
        Dict[host]["ctf htf"] = Dict[host]["ctf htf"] + 1
        try:
            Dict[host]["k/d"] = round(float(Dict[host]["frags"]) / float(Dict[host]["deaths"]),2)
        except:
            Dict[host]["k/d"] = round(float(Dict[host]["frags"]), 2)
    elif size == 8:
        Dict[host]["score"] = Dict[host]["score"] + int(info[2])
        Dict[host]["frags"] = Dict[host]["frags"] + int(info[3])
        Dict[host]["deaths"] = Dict[host]["deaths"] + int(info[4])
        Dict[host]["dm lss osok"] = Dict[host]["dm lss osok"] + 1
        try:
            Dict[host]["k/d"] = round(float(Dict[host]["frags"]) / float(Dict[host]["deaths"]), 2)
        except:
            Dict[host]["k/d"] = round(float(Dict[host]["frags"]), 2)
    elif size == 10:
        Dict[host]["score"] = Dict[host]["score"] + int(info[3])
        Dict[host]["frags"] = Dict[host]["frags"] + int(info[4])
        Dict[host]["deaths"] = Dict[host]["deaths"] + int(info[5])
        Dict[host]["tk"] = Dict[host]["tk"] + int(info[6])
        Dict[host]["tdm tlss tosok"] = Dict[host]["tdm tlss tosok"] + 1
        try:
            Dict[host]["k/d"] = round(float(Dict[host]["frags"]) / float(Dict[host]["deaths"]), 2)
        except:
            Dict[host]["k/d"] = round(float(Dict[host]["frags"]), 2)
    return Dict

def update_hs(host, name, i, Dict):
    Dict = dictionary(Dict, host, name)
    if i == 1:
        Dict[host]["hs"] = Dict[host]["hs"] + 1
    else:
        Dict[host]["hs"] = Dict[host]["hs"] - 1 
    return Dict

def update_sl(host, name, i, Dict):
    Dict = dictionary(Dict, host, name)
    if i == 1:
        Dict[host]["sl"] = Dict[host]["sl"] + 1
    else:
        Dict[host]["sl"] = Dict[host]["sl"] - 1 
    return Dict

def update_gb(host, name, i, Dict):
    Dict = dictionary(Dict, host, name)
    if i == 1:
        Dict[host]["gb"] = Dict[host]["gb"] + 1
    else:
        Dict[host]["gb"] = Dict[host]["gb"] - 1 
    return Dict

def f_returns(host, name, Dict):
    Dict = dictionary(Dict, host, name)
    Dict[host]["flagreturns"] = Dict[host]["flagreturns"] + 1
    return Dict

def update_time(host, name, time, Dict):
    Dict = dictionary(Dict, host, name)
    Dict[host]["time"] = Dict[host]["time"] + int(time)
    return Dict

def write_to(Dict):
    to_json = json.dumps(Dict, indent=4)
    with open("stats.json", "w") as g:
        g.write(to_json)
        g.close()

while 1:
    where = file.tell()
    line = file.readline()
    if not line:
        if(Dict != {}):
            write_to(Dict)
            Dict = {}
        time.sleep(1)
        file.seek(where)
    else:
        if re.match("Game start: ctf on .*, .* players, .* minutes, mastermode 0, .*", line):
            mo = re.search("Game start: ctf on (.*), (.*) players, (.*) minutes, mastermode 0, .*", line)
            if int(mo.group(3)) == 15 or int(mo.group(3)) == 12:
                record_or_not = True
            else:
                record_or_not = False
        elif re.match("Game status: ctf on .+, game finished, open, \d{1,2} clients", line):
            mo = re.search("Game status: ctf on .+, game finished, open, (\d{1,2}) clients", line)
            if int(mo.group(1)) > 4:
                match_ctf = True
            continue
        elif re.match("Demo \".+ .+ .+: ctf, .+, .+\" recorded.", line):
            match_ctf = False
            continue
        elif re.match("Game status: hunt the flag on .+, game finished, open, \d{1,2} clients", line):
            mo = re.search("Game status: hunt the flag on .+, game finished, open, (\d{1,2}) clients", line)
            if int(mo.group(1)) > 4:
                match_ctf = True
            continue
        elif re.match("Demo \".+ .+ .+: hunt the flag, .+, .+\" recorded.", line):
            match_ctf = False
            continue
        elif re.match("Game status: deathmatch on .+, game finished, open, \d{1,2} clients", line):
            mo = re.search("Game status: deathmatch on .+, game finished, open, (\d{1,2}) clients", line)
            if int(mo.group(1)) > 4:
                match_dm = True
            continue
        elif re.match("Demo \".+ .+ .+: deathmatch, .+, .+\" recorded.", line):
            match_dm = False
            continue
        elif re.match("Game status: team deathmatch on .+, game finished, open, \d{1,2} clients", line):
            mo = re.search("Game status: team deathmatch on .+, game finished, open, (\d{1,2}) clients", line)
            if int(mo.group(1)) > 4:
                match_tdm = True
            continue
        elif re.match("Demo \".+ .+ .+: team deathmatch, .+, .+\" recorded.", line):
            match_tdm = False
            continue
        elif re.match("Game status: one shot, one kill on .+, game finished, open, \d{1,2} clients", line):
            mo = re.search("Game status: one shot, one kill on .+, game finished, open, (\d{1,2}) clients", line)
            if int(mo.group(1)) > 4:
                match_dm = True
            continue
        elif re.match("Demo \".+ .+ .+: one shot, one kill, .+, .+\" recorded.", line):
            match_dm = False
            continue
        elif re.match("Game status: team one shot, one kill on .+, game finished, open, \d{1,2} clients", line):
            mo = re.search("Game status: team one shot, one kill on .+, game finished, open, (\d{1,2}) clients", line)
            if int(mo.group(1)) > 4:
                match_tdm = True
            continue
        elif re.match("Demo \".+ .+ .+: team one shot, one kill, .+, .+\" recorded.", line):
            match_tdm = False
            continue
        elif re.match("Game status: last swiss standing on .+, game finished, open, \d{1,2} clients", line):
            mo = re.search("Game status: last swiss standing on .+, game finished, open, (\d{1,2}) clients", line)
            if int(mo.group(1)) > 4:
                match_dm = True
            continue
        elif re.match("Demo \".+ .+ .+: last swiss standing, .+, .+\" recorded.", line):
            match_dm = False
            continue
        elif re.match("Game status: team last swiss standing on .+, game finished, open, \d{1,2} clients", line):
            mo = re.search("Game status: team last swiss standing on .+, game finished, open, (\d{1,2}) clients", line)
            if int(mo.group(1)) > 4:
                match_tdm = True
            continue
        elif re.match("Demo \".+ .+ .+: team last swiss standing, .+, .+\" recorded.", line):
            match_tdm = False
            continue
        elif re.match("\[\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}\]\s.+\sgibbed\stheir\steammate\s.*", line):
            mo = re.search("\[(\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3})\]\s(.+)\sgibbed\stheir\steammate\s.*", line)
            Dict = update_gb(mo.group(1), mo.group(2), 0, Dict)
            continue
        elif re.match("\[\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}\] .+ gibbed .*", line):
            mo = re.search("\[(\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3})\] (.+) gibbed .*", line)
            Dict = update_gb(mo.group(1), mo.group(2), 1, Dict)
            continue
        elif re.match("\[\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}\]\s.+\sslashed\stheir\steammate\s.*", line):
            mo = re.search("\[(\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3})\]\s(.+)\sslashed\stheir\steammate\s.*", line)
            Dict = update_sl(mo.group(1), mo.group(2), 0, Dict)
            continue
        elif re.match("\[\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}\] .+ slashed .*", line):
            mo = re.search("\[(\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3})\] (.+) slashed .*", line)
            Dict = update_sl(mo.group(1), mo.group(2), 1, Dict)
            continue
        elif re.match("\[\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}\]\s.+\sheadshot\stheir\steammate\s.*", line):
            mo = re.search("\[(\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3})\]\s(.+)\sheadshot\stheir\steammate\s.*", line)
            Dict = update_hs(mo.group(1), mo.group(2), 0, Dict)
            continue
        elif re.match("\[\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}\] .+ headshot .*", line):
            mo = re.search("\[(\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3})\] (.+) headshot .*", line)
            Dict = update_hs(mo.group(1), mo.group(2), 1, Dict)
            continue
        elif re.match("\[\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}\] .+ returned the flag", line):
            mo = re.search("\[(\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3})\] (.+) returned the flag", line)
            Dict = f_returns(mo.group(1), mo.group(2), Dict)
            continue
        elif re.match("\[\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}\] disconnected client .+ cn .+, .+ seconds played, score saved", line):
            mo = re.search("\[(\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3})\] disconnected client (.+) cn .+, (.+) seconds played, score saved", line)
            Dict = update_time(mo.group(1), mo.group(2), mo.group(3), Dict)
            continue
        elif match_ctf and record_or_not:
            if re.match("\s|[0-9].*", line):
                if len(line.strip())!= 0:
                    try:
                        cn, name, team, flag, score, frag, death, tk, ping, role, host = line.split()
                    except:
                        continue
                    info = [cn, name, team, flag, score, frag, death, tk, ping, role, host]
                    Dict = update(info, host, name, Dict)
        elif match_dm & record_or_not:
            if re.match("\s|[0-9].*", line):
                if len(line.strip())!= 0:
                    try:
                        cn, name, score, frag, death, ping, role, host = line.split()
                    except:
                        continue
                    info = [cn, name, score, frag, death, ping, role, host]
                    Dict = update(info, host, name, Dict)
        elif match_tdm and record_or_not:
            if re.match("\s|[0-9].*", line):
                if len(line.strip())!= 0:
                    try:
                        cn, name, team,score, frag, death, tk, ping, role, host = line.split()
                    except:
                        continue
                    info = [cn, name, team,score, frag, death, tk, ping, role, host]
                    Dict = update(info, host, name, Dict)
