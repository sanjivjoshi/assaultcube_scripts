<?php

$webhookurl = "https://discord.com/api/webhooks/727822223326445620/R4FyAuy5ba-g7XXxpc6zy35UwUWUAm_0m0lRgseVtsZGF9pkdVuQiauTp8Z42GXO_FQY";

$timestamp = date("c", strtotime("now"));

$json_data = json_encode([
    "content" => "ZZ Powerban hammer!", 
    "username" => "Fats",
    "tts" => false,
    "embeds" => [
        [
            "title" => "has powerbanned ".$argv[2]." reason: ".$argv[3],
            "type" => "rich",
            "description" => "This ban is permanent! Unbanning requires Fatso. As of now, due to IP restrictions its only limited to ZZ members and Jahoo! Usage is '!powerban [FULL_IP_ONLY] [reason]' .This ban is effective immediately.",
            //"url" => "assaultcube://zzgang.servegame.com:3999/?password=match",
            "timestamp" => $timestamp,
            "color" => hexdec( "3366ff" ),
            "footer" => [
                "text" => "fatso"
            ],
            "image" => [
                "url" => ""
            ],
            "author" => [
                "name" => $argv[1]
            ]
        ]
    ]

], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE );


$ch = curl_init( $webhookurl );
curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
curl_setopt( $ch, CURLOPT_POST, 1);
curl_setopt( $ch, CURLOPT_POSTFIELDS, $json_data);
curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt( $ch, CURLOPT_HEADER, 0);
curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);

$response = curl_exec( $ch );
curl_close( $ch );
