import json 
from collections import Counter

f1 = open("stats1.json", "r")
f2 = open("stats2.json", "r")

newDict = {}
Dict1 = json.load(f1)
Dict2 = json.load(f2)
f1.close()
f2.close()

def merge(Dict1, Dict2):
    for key1 in Dict1.keys():
        for key2 in Dict2.keys():
            if str(key1) == str(key2):
                newDict[key1]={"recentname":"", "score":int(0), "frags":int(0), "deaths":int(0), \
                "k/d":float(0), "hs":int(0), "sl":int(0), "gb":int(0), "ctf htf":int(0), \
                    "tdm tlss tosok":int(0), "dm lss osok":int(0), "tk":int(0), "flagreturns": int(0), "flags":int(0), "time":int(0), "country":""}
                newDict[key1]["recentname"] = Dict1[key1]["recentname"] 
                newDict[key1]["score"] = Dict1[key1]["score"] + Dict2[key2]["score"]
                newDict[key1]["frags"] = Dict1[key1]["frags"] + Dict2[key2]["frags"]
                newDict[key1]["deaths"] = Dict1[key1]["deaths"] + Dict2[key2]["deaths"]
                try:
                    newDict[key1]["k/d"] = round(float(newDict[key1]["frags"]) / float(newDict[key1]["deaths"]),2)
                except:
                    newDict[key1]["k/d"] = round(float(newDict[key1]["frags"]), 2)
                newDict[key1]["hs"] = Dict1[key1]["hs"] + Dict2[key2]["hs"]
                newDict[key1]["sl"] = Dict1[key1]["sl"] + Dict2[key2]["sl"]
                newDict[key1]["gb"] = Dict1[key1]["gb"] + Dict2[key2]["gb"]
                newDict[key1]["ctf htf"] = Dict1[key1]["ctf htf"] + Dict2[key2]["ctf htf"]
                newDict[key1]["tdm tlss tosok"] = Dict1[key1]["tdm tlss tosok"] + Dict2[key2]["tdm tlss tosok"]
                newDict[key1]["dm lss osok"] = Dict1[key1]["dm lss osok"] + Dict2[key2]["dm lss osok"]
                newDict[key1]["tk"] = Dict1[key1]["tk"] + Dict2[key2]["tk"]
                newDict[key1]["flagreturns"] = Dict1[key1]["flagreturns"] + Dict2[key2]["flagreturns"]
                newDict[key1]["flags"] = Dict1[key1]["flags"] + Dict2[key2]["flags"]
                newDict[key1]["time"] = Dict1[key1]["time"] + Dict2[key2]["time"]
                newDict[key1]["country"] = Dict1[key1]["country"]
                break
            else:
                newDict[key1]  = Dict1[key1]
    for key2 in Dict2.keys():
        if key2 not in newDict.keys():
            newDict[key2] = Dict2[key2]
    return newDict

f3 = open("stats.json", "w")
merge(Dict1, Dict2)
to_json = json.dumps(newDict, indent=4)
f3.write(to_json)
