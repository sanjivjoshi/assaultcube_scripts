import time
import re
import json

file = open("gameKeeper.txt",)
file1 = open("zz.log",)
match_ctf = False
match_tdm = False
Dict = {}

def toDictionary(playerInfoList):
    length = len(playerInfoList)
    if length == 11:
        host = playerInfoList[10]
        if str(host) not in Dict.keys():
            Dict[str(host)] = {"name":playerInfoList[1], "flags":int(playerInfoList[3]), "score":int(playerInfoList[4]), "headshots": int(0), "frags":int(playerInfoList[5]), "ctfmatches": int(1), "othermatches": int(0)}
        else:
            Dict[str(host)]["score"] = Dict[str(host)]["score"] + int(playerInfoList[4])
            Dict[str(host)]["frags"] = Dict[str(host)]["frags"] + int(playerInfoList[5])
            Dict[str(host)]["flags"] = Dict[str(host)]["flags"] + int(playerInfoList[3])
            Dict[str(host)]["ctfmatches"] = Dict[str(host)]["ctfmatches"] + 1
    if length == 10:
        host = playerInfoList[9]
        if host not in Dict.keys():
            Dict[str(host)] = {"name":playerInfoList[1], "flags":int(0), "score":int(playerInfoList[3]), "headshots": int(0), "frags":int(playerInfoList[4]), "ctfmatches": int(0), "othermatches": int(1)}
        else:
            Dict[str(host)]["score"] = Dict[str(host)]["score"] + int(playerInfoList[3])
            Dict[str(host)]["frags"] = Dict[str(host)]["frags"] + int(playerInfoList[4])
            Dict[str(host)]["othermatches"] = Dict[str(host)]["othermatches"] + 1
    if length == 8:
        host = playerInfoList[7]
        if host not in Dict.keys():
            Dict[str(host)] = {"name":playerInfoList[1], "flags":int(0), "score":int(playerInfoList[2]), "headshots": int(0), "frags":int(playerInfoList[3]), "ctfmatches": int(0), "othermatches": int(1)}
        else:
            Dict[str(host)]["score"] = Dict[str(host)]["score"] + int(playerInfoList[2])
            Dict[str(host)]["frags"] = Dict[str(host)]["frags"] + int(playerInfoList[3])
            Dict[str(host)]["othermatches"] = Dict[str(host)]["othermatches"] + 1

    return Dict

def headshot(ip, name, i):
    if i == 1:
        if str(ip) not in Dict.keys():
            Dict[str(ip)] = {"name":str(name), "flags":int(0), "score":int(0), "headshots": int(1), "frags":int(0), "ctfmatches": int(0), "othermatches": int(0)}
        else:
            Dict[str(ip)]["headshots"] = Dict[str(ip)]["headshots"] + 1
    else:
        if str(ip) not in Dict.keys():
            Dict[str(ip)] = {"name":str(name), "flags":int(0), "score":int(0), "headshots": int(-1), "frags":int(0), "ctfmatches": int(0), "othermatches": int(0)}
        else:
            Dict[str(ip)]["headshots"] = Dict[str(ip)]["headshots"] - 1

while 1:
    where = file.tell()
    where1 = file1.tell()
    line1 = file1.readline()
    line = file.readline()
    if not line and not line1:
        time.sleep(1)
        file.seek(where)
        file1.seek(where1)
    else:
        if re.match("\[(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\]\s*(.*)\s*headshot\s*.*", line1):
            mo = re.search("\[(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\]\s*(.*)\s*headshot\s*.*", line1)
            headshot(mo.group(1), mo.group(2), 1)
        if re.match("\[(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\]\s*(.*)\s*headshot\s*their\s*teammate.*", line1):
            mo = re.search("\[(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\]\s*(.*)\s*headshot\s*their\s*teammate.*", line1)
            headshot(mo.group(1), mo.group(2), 0)
        if re.match("cn\s*name\s*team\s*flag\s*score\s*frag\s*death\s*tk\s*ping\s*role\s*host", line):
            match_ctf = True
            continue
        elif re.match("cn\s*name\s*team\s*score\s*frag\s*death\s*tk\s*ping\s*role\s*host", line):
            match_tdm = True
            continue
        elif re.match("cn\s*name\s*score\s*frag\s*death\s*ping\s*role\s*host", line):
            match_dm = True
            continue
        elif re.match("Team\s*CLA:\s*[0-9]*\s*players,\s*[0-9]*\s*frags,\s*[0-9]*\s*flags", line):
            match_ctf = False
            continue
        elif re.match("Team\s*CLA:\s*[0-9]*\s*players,\s*[0-9]*\s*frags", line):
            match_tdm = False
            continue
        elif re.match("####################################################################################", line):
            match_dm = False
            continue
        if match_ctf:
            if len(line.strip()) != 0:
                if re.match("(\s|[0-9]).*", line):
                    cn, name, team, flag, score, frag, death, tk, ping, role, host = line.split()
                    playerInfoList = [cn, name, team, flag, score, frag, death, tk, ping, role, host]
                    Dict = toDictionary(playerInfoList)
                    to_json = json.dumps(Dict, indent = 4)
                    f = open('stats.json', 'w')
                    f.write(to_json)
                    f.close()
        if match_tdm:
            if len(line.strip()) != 0:
                if re.match("(\s|[0-9]).*", line):
                    cn, name, team, score, frag, death, tk, ping, role, host = line.split()
                    playerInfoList = [cn, name, team, score, frag, death, tk, ping, role, host]
                    Dict = toDictionary(playerInfoList)
                    to_json = json.dumps(Dict, indent = 4)
                    f = open('stats.json', 'w')
                    f.write(to_json)
                    f.close()
        if match_dm:
            if len(line.strip()) != 0:
                if re.match("(\s|[0-9]).*", line):
                    cn, name, score, frag, death, ping, role, host = line.split()
                    playerInfoList = [cn, name, score, frag, death, ping, role, host]
                    Dict = toDictionary(playerInfoList)
                    to_json = json.dumps(Dict, indent = 4)
                    f = open('stats.json', 'w')
                    f.write(to_json)
                    f.close()
