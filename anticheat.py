import os, time, re
thefile = open('zz1.log', 'r')
thefile.seek(0,2)
while True:
	line = thefile.readline() 
	if not line:
		time.sleep(0.1)
		continue
	else:
		if re.match("\[(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\]\ * tried to touch the * flag at distance*", line):
			mo = re.search("\[(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\]\ * tried to touch the (*) flag at distance*", line) 
			cmd = "iptables -I INPUT -s "+mo.group(1)+" -j DROP"
			os.system(cmd)
		elif re.match("\[(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\]\ * is colliding with the map", line):
			mo = re.search("\[(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\]\ * is colliding with the map*", line) 
			cmd = "iptables -I INPUT -s "+mo.group(1)+" -j DROP"
			os.system(cmd)
