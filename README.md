# AssaultCube Scripts by ZZ|Fatso (sanjivjoshi) written in Python

The scripts were created as a hobby after playing the game for a very long and 
joining zzgang.

The scripts' meaning 

*_ddos.py : All these scripts are mitigating dos attacks to the game. Although 
the server has DDoS protection, the socket security was missing, therefore had to
analyse the log and app an IP to iptables found to be effective. Players making more
than 4 connections to the server are either DoS or need consent from admin to host
LAN parties.

scorekeeper.py : effectively notes down the score to a file and helps in further 
analysis. This file is called gameKeeper.txt and notes all player scores at the end
of the game for CTF, TOSOK, OSOK, DM, TDM and LSS

statskeeper.py : effectively maintains a json of player stats regualary updating 
for a reference. This file might just add info for a html page in the future. Good 
feature is mainly that headshots are noted as well.