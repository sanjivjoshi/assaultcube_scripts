<?php

$webhookurl = "https://discord.com/api/webhooks/727822223326445620/R4FyAuy5ba-g7XXxpc6zy35UwUWUAm_0m0lRgseVtsZGF9pkdVuQiauTp8Z42GXO_FQY";

$timestamp = date("c", strtotime("now"));

$json_data = json_encode([
    "content" => "Someone has reported something unusual", 
    "username" => "Fats",
    "tts" => false,
    "embeds" => [
        [
            "title" => "has reported that ".$argv[2]." seems to be ".$argv[3],
            "type" => "rich",
            "description" => "The report has been generated and action needs to be taken. For future reports pls use '!report [IP|Name] [reason]'",
            //"url" => "assaultcube://zzgang.servegame.com:3999/?password=match",
            "timestamp" => $timestamp,
            "color" => hexdec( "3366ff" ),
            "footer" => [
                "text" => "fatso"
            ],
            "image" => [
                "url" => ""
            ],
            "author" => [
                "name" => $argv[1]
            ]
        ]
    ]

], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE );


$ch = curl_init( $webhookurl );
curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
curl_setopt( $ch, CURLOPT_POST, 1);
curl_setopt( $ch, CURLOPT_POSTFIELDS, $json_data);
curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt( $ch, CURLOPT_HEADER, 0);
curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);

$response = curl_exec( $ch );
curl_close( $ch );
