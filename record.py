import os.path, re, IP2Location, time, json, threading

#Initialisations
IP2LocObj = IP2Location.IP2Location()
IP2LocObj.open("/home/assaultcube/Common-Data/IP2Location/IP-COUNTRY.BIN")
record_hs = False
record_frags = False
record_or_not = False
Dict = {}
allowed_maps = []
file = open("zz1.log", "r")
file.seek(0,2)

#Functions
def setcountry(rec):
    if rec is not None:
        try:
            return rec.country_short
        except:
            return ""
    else:
        return ""

def dictionary(Dict, host, name):
    global allowed_maps
    allowed_maps = []
    with open("map.txt", 'r') as mapfile:
        for line in mapfile:
            allowed_maps.append(line.strip())
    rec = IP2LocObj.get_all(host)
    if os.path.exists('record.json') and Dict=={}:
        json_obj = open('record.json')
        Dict = json.load(json_obj)
        if host in Dict.keys():
            Dict[host]["name"] = name
            return Dict
        else:
            Dict[host] = {"name":name, "record_hs":{}, "hs":int(0), "record_frags":{}, "record_sc":{}, "country":setcountry(rec),\
                "record_flag":{}, "kd":{}}
            return Dict
    elif host in Dict.keys():
        Dict[host]["name"] = name
        return Dict
    else:
        Dict[host] = {"name":name, "record_hs":{}, "hs":int(0), "record_frags":{}, "record_sc":{}, "country":setcountry(rec),\
                "record_flag":{}, "kd":{}}
    return Dict

def check_record(host, Dict, mapp):
    if Dict[host]["hs"] > 10:
        if  mapp in allowed_maps:
            if mapp in Dict[host]["record_hs"].keys():
                if  Dict[host]["record_hs"][str(mapp)] < Dict[host]["hs"]:
                    Dict[host]["record_hs"][str(mapp)] = Dict[host]["hs"]
            else:
                Dict[host]["record_hs"][str(mapp)] = Dict[host]["hs"]
    return Dict

def update_hs(host, name, count, Dict, mapp):
    Dict = dictionary(Dict, host, name)
    if count == 1:
        Dict[host]["hs"] = Dict[host]["hs"] + 1
    else:
        Dict[host]["hs"] = Dict[host]["hs"] - 1
    Dict = check_record(host, Dict, mapp) 
    return Dict

def update_frag(host, name, frag, Dict, mappp):
    Dict = dictionary(Dict, host, name)
    if mappp in allowed_maps:
        if mappp in Dict[host]["record_frags"].keys():
                if  Dict[host]["record_frags"][str(mappp)] < int(frag):
                    Dict[host]["record_frags"][str(mappp)] = int(frag)
        else:
            Dict[host]["record_frags"][str(mappp)] = int(frag)
    return Dict

def update_sc(host, name, score, Dict, mappp):
    Dict = dictionary(Dict, host, name)
    if mappp in allowed_maps:
        if mappp in Dict[host]["record_sc"].keys():
                if  Dict[host]["record_sc"][str(mappp)] < int(score):
                    Dict[host]["record_sc"][str(mappp)] = int(score)
        else:
            Dict[host]["record_sc"][str(mappp)] = int(score)
    return Dict

def reset_local(Dict):
    for key in Dict.keys():
        Dict[key]["hs"] = 0
    return Dict

def update_flag(host, name, flag, Dict, mappp):
    Dict = dictionary(Dict, host, name)
    if mappp in allowed_maps:
        if mappp in Dict[host]["record_flag"].keys():
                if  Dict[host]["record_flag"][str(mappp)] < int(flag):
                    Dict[host]["record_flag"][str(mappp)] = int(flag)
        else:
            Dict[host]["record_flag"][str(mappp)] = int(flag)
    return Dict

def update_kd(host, name, frag, death, Dict, mappp):
    Dict = dictionary(Dict, host, name)
    if int(death) != 0:
        kd = round(float(frag)/float(death), 2)
    else:
        kd = round(float(frag), 2)
    if mappp in allowed_maps:
        if mappp in Dict[host]["kd"].keys():
                if  Dict[host]["kd"][str(mappp)] < kd:
                    Dict[host]["kd"][str(mappp)] = kd
        else:
            Dict[host]["kd"][str(mappp)] = kd
    return Dict

def write_to(Dict):
    to_json = json.dumps(Dict, indent=4)
    f = open('record.json', 'w')
    f.write(to_json)
    f.close()

#main driver
while 1:
    where = file.tell()
    line = file.readline()
    if not line:
        if(Dict != {}):
            write_to(Dict)
            Dict = {}
        time.sleep(1)
        file.seek(where)
    else:
        if re.match("Game start: ctf on .*, .* players, .* minutes, mastermode 0, .*", line):
            mo = re.search("Game start: ctf on (.*), (.*) players, (.*) minutes, mastermode 0, .*", line)
            if int(mo.group(3))>10:
                record_or_not = True
            else:
                record_or_not = False
        if re.match("Game start: .+ on .+, .+ players, .+", line):
            mo = re.search("Game start: (.+) on (.+), (\d{1,2}) players, .+", line)
            if int(mo.group(3)) > 7:
                record_hs = True
                mapp = mo.group(2)
            continue
        elif re.match("Game status: .+ on .+, game finished, open, \d{1,2} clients", line):
            record_hs = False
            Dict = reset_local(Dict)
            mo = re.search("Game status: .+ on (.+), game finished, open, (\d{1,2}) clients", line)
            if int(mo.group(2)) > 7:
                mappp = mo.group(1)
                record_frags = True
            continue
        elif re.match("Demo \".+ .+ .+: .+, .+, .+\" recorded.", line):
            record_hs = False
            Dict = reset_local(Dict)
            record_frags = False
            continue
        elif re.match("Demo discarded.", line):
            record_hs = False
            Dict = reset_local(Dict)
            record_frags = False
            continue
        elif record_hs and record_or_not:
            if re.match("\[\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}\] .{1,15} headshot their teammate .+", line):
                mo = re.search("\[(\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3})\] (.+) headshot their teammate .+", line)
                Dict = update_hs(mo.group(1), mo.group(2), 0, Dict, mapp)
            elif re.match("\[\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}\] .{1,15} headshot .+", line):
                mo = re.search("\[(\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3})\] (.+) headshot .+", line)
                Dict = update_hs(mo.group(1), mo.group(2), 1, Dict, mapp)
            continue
        elif record_frags and record_or_not:
            if re.match("\s|[0-9].*", line):
                if len(line.strip())!= 0:
                    val = list(line.split())
                    if len(val) == 11:
                        cn, name, team, flag, score, frag, death, tk, ping, role, host = line.split()
                        Dict = update_flag(host,name, flag, Dict, mappp)
                    elif len(val) == 8:
                        cn, name, score, frag, death, ping, role, host = line.split()
                    elif len(val) == 10:
                        cn, name, team,score, frag, death, tk, ping, role, host = line.split()
                    Dict = update_frag(host, name, frag, Dict, mappp)
                    Dict = update_sc(host, name, score, Dict, mappp)
                    Dict = update_kd(host, name, frag, death, Dict, mappp)
                    
