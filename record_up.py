import json
import os.path
import datetime
from operator import itemgetter

# init
buffer = ""
player = []

# main driver
if os.path.exists('record.json'):
    f = open('record.json', )
    toDict = json.load(f)
    f.close()
    for host in toDict.keys():
        player.append({
            "country": toDict[host]["country"],
            "name": toDict[host]["name"],
            "score": toDict[host]["record_sc"],
            "map_sc": toDict[host]["map_sc"],
            "record_frags": toDict[host]["record_frags"],
            "map_fr": toDict[host]["map_fr"],
            "record_hs": toDict[host]["record_hs"],
            "hs_map": toDict[host]["hs_map"],
            "record_flag": toDict[host]["record_flag"],
            "map_fl": toDict[host]["map_fl"],
            "kd": toDict[host]["kd"],
            "map_kd": toDict[host]["map_kd"]
        })
    data_sorted = sorted(player, key=itemgetter("score"), reverse=True)
    count = 0
    buffer = \
        """
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title></title> 
  <link rel="stylesheet" href="css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="css/buttons.dataTables.css">
  <style>
    th,td{
      text-align: center; 
    }
    body {
      max-width: 50%;
      margin: auto;
    }
    div.dataTables_wrapper {
      margin-bottom: 3em;
    }
  </style>
</head>
<body>
  <h2> Scorers! </h2>
  <p> <b> You ask scores? Well these players know no bounds of it. They stop at nothing! </b> </p> 
  <table id="" class="display" width="100%">
    <thead>
      <tr>
        <th>Country
        </th>
        <th>Name
        </th>
        <th>Score
        </th>
        <th>Map
        </th>
        </tr>
    </thead>
    <tbody>
"""
    
    for sdic in data_sorted:
        if str(sdic["map_sc"]) in ('ac_casa', 'ac_village', 'ac_ingress', 'ac_africa', 'ac_syria_3.0', 'ac_shine', 'ac_casa', 'ac_desert3', 'ac_douzectf', 'rapier', 'ac_sunset'):
            count = count + 1
            buffer = str(buffer) +\
                "<tr><td>"+str(sdic["country"]) +\
                "</td><td>"+str(sdic["name"]) +\
                "</td><td>"+str(sdic["score"]) +\
                "</td><td>"+str(sdic["map_sc"]) +\
                "</td></tr>"
            if count > 24:
                break
    buffer = str(buffer) +\
        """
</tbody>
  </table>
  <h2> Headshotters! </h2>
  <p> <b> These players don't see anything in their game but heads! Put a hole in the head? These are some sharpshooters you'll love! </b></p>
  <table id="" class="display" width="100%">
    <thead>
      <tr>
        <th>Country
        </th>
        <th>Name
        </th>
        <th>Headshots
        </th>
        <th>Map
        </th>
        </tr>
    </thead>
    <tbody>
"""
    data_sorted = sorted(player, key=itemgetter("record_hs"), reverse=True)
    count = 0
    for sdic in data_sorted:
        if str(sdic["hs_map"]) in ('ac_casa', 'ac_village', 'ac_ingress', 'ac_africa', 'ac_syria_3.0', 'ac_shine', 'ac_casa', 'ac_desert3', 'ac_douzectf', 'rapier', 'ac_sunset'):
            count = count + 1
            buffer = str(buffer) +\
                "<tr><td>"+str(sdic["country"]) +\
                "</td><td>"+str(sdic["name"]) +\
                "</td><td>"+str(sdic["record_hs"]) +\
                "</td><td>"+str(sdic["hs_map"]) +\
                "</td></tr>"
            if count > 24:
                break
    buffer = str(buffer) +\
        """
  </tbody>
  </table>
  <h2> Fraggers! </h2>
  <p><b> I can still hear the bullet shells fall on the floor. You don't wanna mess as well as miss being killed by these players! </b></p>
  <table id="" class="display" width="100%">
    <thead>
      <tr>
        <th>Country
        </th>
        <th>Name
        </th>
        <th>Frags
        </th>
        <th>Map
        </th>
        </tr>
    </thead>
    <tbody>
"""
    data_sorted = sorted(player, key=itemgetter("record_frags"), reverse=True)
    count = 0
    for sdic in data_sorted:
        if str(sdic["map_fr"]) in ('ac_casa', 'ac_village', 'ac_ingress', 'ac_africa', 'ac_syria_3.0', 'ac_shine', 'ac_casa', 'ac_desert3', 'ac_douzectf', 'rapier', 'ac_sunset'):
            count = count + 1
            buffer = str(buffer) +\
                "<tr><td>"+str(sdic["country"]) +\
                "</td><td>"+str(sdic["name"]) +\
                "</td><td>"+str(sdic["record_frags"]) +\
                "</td><td>"+str(sdic["map_fr"]) +\
                "</td></tr>"
            if count > 24:
                break
    buffer = str(buffer) +\
        """
  </tbody>
  </table>
  <h2> Flaggers! </h2>
  <p><b> Cheers to these flag runners! Don't know how we ever won the CTF matches without these players!</b></p>
  <table id="" class="display" width="100%">
    <thead>
      <tr>
        <th>Country
        </th>
        <th>Name
        </th>
        <th>Flags
        </th>
        <th>Map
        </th>
        </tr>
    </thead>
    <tbody>
"""
    data_sorted = sorted(player, key=itemgetter("record_flag"), reverse=True)
    count = 0
    for sdic in data_sorted:
        if str(sdic["map_fr"]) in ('ac_casa', 'ac_village', 'ac_ingress', 'ac_africa', 'ac_syria_3.0', 'ac_shine', 'ac_casa', 'ac_desert3', 'ac_douzectf', 'rapier', 'ac_sunset'):
            count = count + 1
            buffer = str(buffer) +\
                "<tr><td>"+str(sdic["country"]) +\
                "</td><td>"+str(sdic["name"]) +\
                "</td><td>"+str(sdic["record_flag"]) +\
                "</td><td>"+str(sdic["map_fl"]) +\
                "</td></tr>"
            if count > 24:
                break
    buffer = str(buffer) +\
"""
  </tbody>
  </table>
  <h2> K/Ds! </h2>
  <p><b> Nerves of steel! You say 241 or nothing? How about some everything or nothing?</b></p>
  <table id="" class="display" width="100%">
    <thead>
      <tr>
        <th>Country
        </th>
        <th>Name
        </th>
        <th>K/D
        </th>
        <th>Map
        </th>
        </tr>
    </thead>
    <tbody>
"""
    data_sorted = sorted(player, key=itemgetter("kd"), reverse=True)
    count = 0
    for sdic in data_sorted:
        if str(sdic["map_kd"]) in ('ac_casa', 'ac_village', 'ac_ingress', 'ac_africa', 'ac_syria_3.0', 'ac_shine', 'ac_casa', 'ac_desert3', 'ac_douzectf', 'rapier', 'ac_sunset'):
            count = count + 1
            buffer = str(buffer) +\
                "<tr><td>"+str(sdic["country"]) +\
                "</td><td>"+str(sdic["name"]) +\
                "</td><td>"+str(sdic["kd"]) +\
                "</td><td>"+str(sdic["map_kd"]) +\
                "</td></tr>"
            if count > 24:
                break
    buffer = str(buffer) +\
"""
  </tbody>
  </table>
  <!-- End your project here-->
  <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
  <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="js/dataTables.buttons.min.js"></script>
  <script type="text/javascript" src="js/buttons.colVis.min.js"></script>
  <script type="text/javascript" src="js/dataTables.fixedHeader.js"></script>
  <script>
    $(document).ready(function() {
    $("table.display").DataTable( {
        "order": []
    } );
} );
  </script>
</body>

</html>
"""
    f1 = open("/var/www/html/record.html", "w+")
    f1.write(buffer)
    f1.close()
