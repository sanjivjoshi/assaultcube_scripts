<?php

$webhookurl = "https://discordapp.com/api/webhooks/700996131705847828/bFkiWOvAS7E1QsWYb3MzuHMRR_ScMYH9yQqc45R5N2LNB1q6p1r4CCvFJzxXhNve_cEa";

$timestamp = date("c", strtotime("now"));

$json_data = json_encode([
    "content" => "Intra @everyone", 
    "username" => "Fats",
    "tts" => false,
    "embeds" => [
        [
            "title" => "Calls for intra on ZZ private server",
            "type" => "rich",
            "description" => "Type /connect zzgang.servegame.com 1999 zzwho?",
            "timestamp" => $timestamp,
            "color" => hexdec( "3366ff" ),
            "footer" => [
                "text" => "fatso"
            ],
            "author" => [
                "name" => $argv[1]
            ]
        ]
    ]

], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE );


$ch = curl_init( $webhookurl );
curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
curl_setopt( $ch, CURLOPT_POST, 1);
curl_setopt( $ch, CURLOPT_POSTFIELDS, $json_data);
curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt( $ch, CURLOPT_HEADER, 0);
curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);

$response = curl_exec( $ch );
curl_close( $ch );