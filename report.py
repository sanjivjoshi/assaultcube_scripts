import time
import re
import os

file = open('zz1.log', 'r')
playerDict = {}
file.seek(0,2)
def report(reporter, reported, reason):
    cmd = 'php /home/assaultcube/Common-Data/report.php \"'+reporter+'\" '+' \"'+reported+'\" '+'\"'+reason+'\"'
    os.system(cmd)

while 1:
    where = file.tell()
    line = file.readline()
    if not line:
        time.sleep(1)
        file.seek(where)
    else:
        if re.match("\[(\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3})\] (.+) says:\ '!report (.+) (.+)'", line):
            mo = re.search("\[(\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3})\] (.+) says:\ '!report (.+) (.+)'", line)
            if mo.group(1) not in playerDict.keys():
                playerDict[mo.group(1)] = 1
            else:
                playerDict[mo.group(1)] += 1
            if playerDict[mo.group(1)] < 3:
                report(mo.group(2), mo.group(3), mo.group(4))
            continue
        elif re.match("Game status: .+ on .+, game finished, .+, .*", line):
            playerDict = {}
            continue
        elif re.match("Demo \".+ .+ .+: .+, .+, .+\" recorded.", line):
            playerDict = {}
            continue
