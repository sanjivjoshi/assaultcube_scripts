import time
import re
import os

file = open('/home/sanjiv/assaultcubezz/public_1.log', 'r')
iptables = dict()
#admins = list('124.123.82.21')

while 1:
    where = file.tell()
    line = file.readline()
    if not line:
        time.sleep(1)
        file.seek(where)
    else:
        mo = re.search("\[(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\]\ client connected", line)
        if mo:
            if(mo.group(1) not in iptables.keys()):
                iptables[str(mo.group(1))] = 1
            else:
                iptables[str(mo.group(1))] = iptables.get(str(mo.group(1))) + 1
                if(iptables.get(str(mo.group(1))) >= 4):
                    cmd = 'iptables -A INPUT -s '+mo.group(1)+' -j DROP'
                    iptables[mo.group(1)] = 0
                    os.system(cmd)

        mo = re.search("\[(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\]\ disconnected\ .*",line)
        if mo:
            if(mo.group(1) in iptables.keys()):
                iptables[str(mo.group(1))] = iptables.get(str(mo.group(1))) - 1

	mo = re.search("\[(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\]\ disconnecting\ .*",line)
        if mo:
            if(mo.group(1) in iptables.keys()):
                iptables[str(mo.group(1))] = iptables.get(str(mo.group(1))) - 1

	"""mo = re.search("\[(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\]\ (.*)\ says:\ '@report\ (.*)'", line)
	if mo:
	    if(mo.group(1) in admin):
		cmd = 'iptables -A INPUT -s '+mo.group(1)+' -j DROP'
		os.system(cmd)"""
